# Sprint Project Setup
Project setup for Sprint. [Sprint minimal setup](sprint-install.md) must be completed first.


### Configure Sprint
Create, Modify or Delete Files & Directories:

- Delete `/themes/foundation5`

- Modify `/application/config/application.php`
    - Edit `$config['site.name']        = 'Exceptionally Timed';`
    - Edit `$config['site.auth_email']  = 'jlareaux@godaddy.com'`
    - Edit `$config['show_profiler'] = false`
    - Edit `$config['use_php_error'] = false;`
    - Delete `'foundation' => FCPATH .'themes/foundation5',`

- Modify `/application/config/auth.php`
    - Edit `$config['auth.default_role_id'] = 1;`

- Modify `/application/config/autoload.php.php`
    - Edit `$autoload['libraries'] = array('database');`

- Create `/application/config/database.php`
    - Add `$db['wamp'] = array(
               'hostname' => 'localhost',
               'username' => 'root',
               'password' => '',
               'database' => 'catt',
               'dbdriver' => 'mysqli',
               'dbprefix' => 'dev_',
               'db_debug' => true,
               'swap_pre' => 'catt_',
               'stricton' => true,
               'save_queries' => true
           );`


- Modify `/application/config/migration.php`
    - Edit `$config['migration_enabled'] = false;`

- Create `/application/config/development/application.php`
    - Add `$config['auto_migrate'] = array('app');`
    - Add `$config['show_profiler'] = true;`
    - Add `$config['use_php_error'] = true;`

- Create `/application/config/development/config.php`
    - Add `$config['composer_autoload'] = true;`
    - Add `$config['log_threshold'] = 4;`

- Create `/application/config/development/index.html`

- Create `/application/config/development/profiler.php`
    - Add `$config['benchmarks']           = TRUE;
           $config['config']               = TRUE;
           $config['controller_info']      = TRUE;
           $config['get']                  = TRUE;
           $config['http_headers']         = TRUE;
           $config['memory_usage']         = TRUE;
           $config['post']                 = TRUE;
           $config['queries']              = TRUE;
           $config['uri_string']           = TRUE;
           $config['session_data']         = TRUE;
           $config['query_toggle_count']   = TRUE;`

- Create `/application/config/production`

- Create `/application/config/production/index.html`

- Create `/application/config/testing/index.html`

- Create `/application/config/travis/index.html`

- Create `/application/views/profiler_template.php`
    - Edit `$bar_location = 'bottom-right';`
    - Edit `#ci-profiler-menu-exit { padding-bottom: 28px !important; }`
    - Edit `#ci-profiler-menu-open.bottom-right { position: fixed; right: 0px; bottom: 11px; }`
    - Edit `#ci-profiler-menu-open.bottom-left { position: fixed; left: 8px; bottom: 11px; }`
    - Edit `#ci-profiler-menu-open.top-left { position: fixed; left: 8px; top: 11px; }`
    - Edit `#ci-profiler-menu-open.top-right { position: fixed; right: 0px; top: 11px; }`

- Edit `/application/database/seeds/FlatAuthorizationSeeder.php`
    - Edit `// User Groups
            $flat->createGroup('user', 'Users');
            $flat->createGroup('admin', 'Administrators');

            // User Account Permissions
            $flat->createPermission('viewUser', 'View your profile.');
            $flat->createPermission('manageUser', 'Edit your profile.');
            $flat->createPermission('viewOtherUsers', 'View other users.');
            $flat->createPermission('manageOtherUsers', 'Manage other users.');

            // User Privileges
            $flat->addPermissionToGroup('viewUser', 'user');
            $flat->addPermissionToGroup('manageUser', 'user');
            // Admin Privileges
            $flat->addPermissionToGroup('viewUser', 'admin');
            $flat->addPermissionToGroup('manageUser', 'admin');
            $flat->addPermissionToGroup('viewOtherUsers', 'admin');
            $flat->addPermissionToGroup('manageOtherUsers', 'admin');`


### Seed the Database

```bash
php sprint database seed FlatAuthorizationSeeder
```
