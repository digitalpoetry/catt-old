# Sprint


- [GIT Keyword expansion](http://forum.diyefi.org/viewtopic.php?f=45&t=2117).
- [Version numbering](http://thomas-cokelaer.info/blog/2014/12/moving-from-svn-to-git-id-equivalent/) with GIT. 
- [See Also](http://stackoverflow.com/questions/16524225/how-can-i-populate-the-git-commit-id-into-a-file-when-i-commit#answer-16524375)
- [here too](https://wincent.com/blog/automatic-deployment-numbering-in-git)
- [lastly](https://www.quora.com/How-do-I-include-Git-commit-hash-in-managed-source-code).
- [PHP Annotations ](http://php-annotations.readthedocs.io/en/latest/ConsumingAnnotations.html)



## Time Exceptions
Based on the [Simple Blog](http://x.ly/docs/developer/tutorials/blog) tutorial.


### Create Time Exceptions Migration
See [Reference](http://x.ly/docs/developer/forge/generators#migration) migration.
Generates:
 - application/database/migrations/XXXX_Create_xceptions_table.php

```bash
cd xlt
php sprint forge migration create_xceptions_table -fields "\
id:id \
created_by:number \
supervisor:number \
start:datetime \
end:datetime \
authorized_by:number \
reason:string \
approved:tinyint:1 \
approved_by:number \
approved_on:datetime \
created_on:datetime \
modified_on:datetime \
deleted:tinyint:1"
```


### Migrate Time Exceptions

```bash
php sprint database migrate
```


### Scaffold Time Exceptions
Generates:
- application/database/seeds/XceptionsSeeder.php - An empty seed file we will use to enter generic test data
    - Customize, See [Reference](http://x.ly/index.php/docs/developer/database/seeding) Seeding.
- application/models/Xception_model.php - Our base model based on CIDbModel that provides many useful methods to work with our database table.
    - Customize, See [Reference](http://www.codeigniter.com/userguide3/libraries/form_validation.html#rule-reference) Form Validation.
- application/controllers/Xceptions.php - A controller that handles the basic CRUD operations for us.
    - Customize, See [Reference](http://x.ly/docs/developer/tutorials/blog#admin_controller) Admin Controller.
- application/views/xceptions/index.php - View to list all time exceptions
    - Customize
- application/views/xceptions/create.php - View with the creation form for new time exceptions
    - Customize
- application/views/xceptions/show.php - View to show a single form
    - Customize
- application/views/xceptions/update.php - View with the editing form for existing time exceptions
    - Customize

```bash
php sprint forge scaffold xceptions -overwrite
```


### Route Time Exceptions to Admin Area
Create(+)/Modify(*)/Delete(-) Files/Directories:
+ `/application/libraries/AdminController.php`
    - See [Reference](http://x.ly/docs/developer/tutorials/blog#admin_controller) Admin Controller.
* `/application/controllers/Xceptions.php`
    - See [Reference](http://x.ly/docs/developer/tutorials/blog#admin_controller) Admin Controller.
* `/application/config/routes.php`
    - See [Reference](http://x.ly/docs/developer/tutorials/blog#admin_area) Admin Area.


### The _ Controller
Forge using the `xception_model`

```bash
php sprint forge controller _
```


### The _ View
See [Reference](http://x.ly/docs/developer/tutorials/blog#the_view) the View.
Create(+)/Modify(*)/Delete(-) Files/Directories:
+ `/application/views/_/index.php`


### User Roles and Capabilities
- Modify `/application/database/seeds/FlatAuthorizationSeeder.php`
    - Edit `// User Groups
       $flat->createGroup('agent', 'Agents');
       $flat->createGroup('super', 'Supervisors');
       $flat->createGroup('admin', 'Administrators');
       $flat->createGroup('dev', 'Developers');

       // Time Exception Permissions 
       $flat->createPermission('viewXceptions', 'View your time exceptions.');
       $flat->createPermission('manageXceptions', 'Manage your time exceptions.');
       $flat->createPermission('viewOthersXceptions', 'View others time exceptions.');
       $flat->createPermission('manageOthersXceptions', 'Manage others time exceptions.');
       // User Account Permissions
       $flat->createPermission('viewUser', 'View your user profile.');
       $flat->createPermission('manageUser', 'Edit your user profile.');
       $flat->createPermission('viewOtherUsers', 'View other user profiles.');
       $flat->createPermission('manageOtherUsers', 'Manage other user profiles.');

       // Developer Privileges
       $flat->addPermissionToGroup('viewXceptions', 'dev');
       $flat->addPermissionToGroup('manageXceptions', 'dev');
       $flat->addPermissionToGroup('viewOthersXceptions', 'dev');
       $flat->addPermissionToGroup('manageOthersXceptions', 'dev');
       $flat->addPermissionToGroup('viewUser', 'dev');
       $flat->addPermissionToGroup('manageUser', 'dev');
       $flat->addPermissionToGroup('viewOtherUsers', 'dev');
       $flat->addPermissionToGroup('manageOtherUsers', 'dev');
       // Admin Privileges
       $flat->addPermissionToGroup('viewOthersXceptions', 'admin');
       $flat->addPermissionToGroup('manageOthersXceptions', 'admin');
       $flat->addPermissionToGroup('viewUser', 'admin');
       $flat->addPermissionToGroup('manageUser', 'admin');
       $flat->addPermissionToGroup('viewOtherUsers', 'admin');
       $flat->addPermissionToGroup('manageOtherUsers', 'admin');
       // Supervisor Privileges
       $flat->addPermissionToGroup('viewOthersXceptions', 'super');
       $flat->addPermissionToGroup('manageOthersXceptions', 'super');
       $flat->addPermissionToGroup('viewUser', 'super');
       $flat->addPermissionToGroup('manageUser', 'super');
       // Agent Privileges
       $flat->addPermissionToGroup('viewXceptions', 'agent');
       $flat->addPermissionToGroup('manageXceptions', 'agent');`


### Site Structure

> x.ly/                     home/index.php
> x.ly/admin/xceptions/     xceptions/index.php
>                     /manage        /manage.php
>                     /create        /create.php
>                     /view          /view.php
>                     /update        /update.php
> x.ly/admin/review/        review/index.php
>                  /team          /team.php
>                  /agent         /agent.php
> x.ly/admin/reports/       reports/index.php
>                   /team          /team.php
>                   /agent         /agent.php
> x.ly/admin/recover/       recover/index.php
>                   /team          /team.php
>                   /agent         /agent.php
> x.ly/admin/user/
>                /profile
>                /settings
