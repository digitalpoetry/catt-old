# Sprint Minimal Setup
The minimum setup required for Sprint. The `base_url` must end with `.dev` or 
the development configuration will not be loaded.


### Install with Composer
See [Reference](https://github.com/ci-bonfire/Sprint/blob/develop/myth/_docs_src/installation.md) installation.

Automatic install:
```bash
php composer.phar create-project sprintphp/sprintphp xlt dev-develop
```


Manual install:
```bash
composer install
composer dump-autoload
php build/build.php postCreateProject
composer dump-autoload -o
```


### Configure Sprint
Create, Modify or Delete Files & Directories:

- Modify `/application/config/autoload.php.php`
    - Edit `$autoload['libraries'] = array('database');`

- Modify `/application/config/database.php`
    - Add `$db['wamp'] = array(
               'hostname' => 'localhost',
               'username' => 'root',
               'password' => '',
               'database' => 'catt',
               'dbdriver' => 'mysqli',
               'dbprefix' => 'catt_',
               'db_debug' => true,
               'swap_pre' => 'dev_',
               'stricton' => true,
           );`

- Create `/application/config/development`

- Create `/application/config/development/auth.php`
    - Add `$config['auth.min_password_strength'] = 8;`
    - Add `$config['auth.hash_cost'] = 4;`

- Create `/application/config/development/config.php`
    - Add `$config['base_url'] = 'http://xlt.dev';`

- Create `/application/config/development/database.php`
    - Add `$active_group = 'wamp';`
    - Add `$query_builder = true;`
    - Add `$db['wamp'] = array(
               'hostname' => 'localhost',
               'username' => 'root',
               'password' => '',
               'database' => 'catt',
               'dbdriver' => 'mysqli',
               'dbprefix' => 'dev_',
               'db_debug' => true,
               'swap_pre' => 'catt_',
               'save_queries' => true
           );`

- Create `/application/config/development/migration.php`
    - Add `$config['migration_create_table_attr'] = [
               'ENGINE' => 'InnoDB'
           ];`


### Migrate the Database

```bash
php sprint database migrate
```


### Login
Signup and then login to Sprint.